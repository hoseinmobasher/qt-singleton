#ifndef MOUSEEVENTHELPER_H
#define MOUSEEVENTHELPER_H


class MouseEventHelper
{
private:
    MouseEventHelper();

public:
    // Access via this function
    static MouseEventHelper *getInstance();

public:
    bool isRightClicked();
    void setRightClicked(bool rightClick);

private:
    static MouseEventHelper *instance;
    bool rightClick;
};

#endif // MOUSEEVENTHELPER_H
