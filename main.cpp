#include <QCoreApplication>
#include <QDebug>

#include "MouseEventHelper.h"

int main(int argc, char *argv[])
{
    MouseEventHelper::getInstance()->setRightClicked(true);

    qDebug() << "Is right clicked? " << MouseEventHelper::getInstance()->isRightClicked();
}

