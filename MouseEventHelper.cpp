#include "MouseEventHelper.h"

MouseEventHelper *MouseEventHelper::instance = 0; // null

MouseEventHelper::MouseEventHelper()
{

}

MouseEventHelper *MouseEventHelper::getInstance()
{
    if (instance == 0)
    {
        instance = new MouseEventHelper();
    }

    return instance;
}

bool MouseEventHelper::isRightClicked()
{
    return this->rightClick;
}

void MouseEventHelper::setRightClicked(bool rightClick)
{
    this->rightClick = rightClick;
}
