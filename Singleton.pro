QT += core
QT -= gui

TARGET = Singleton
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    MouseEventHelper.cpp

HEADERS += \
    MouseEventHelper.h

